/**
 * GameLayer ��� ������� ���� ���� ���������� ��� �������� ���� � UI.
 */
STATE_PLAYING = 0;
STATE_GAMEOVER = 1;
MAX_CONTAINT_WIDTH = 200;
MAX_CONTAINT_HEIGHT = 200;

var g_sharedGameLayer;

var GameLayer = cc.Layer.extend({
    _main: null,
    _progressBack: null,
    _progressBar: null,
    _state: STATE_PLAYING,
    _uiBatch: null,
    _scoreSprite: null,
    _lbScore: null,
    _clicksSprite: null,
    _lbClicks: null,
    _gameoverSprite: null,
    _lbGameover: null,
    _lbScoreGameover: null,
    _pauseSprite: null,
    explosionAnimation: [],
    _explosions: null,
    _tmpScore: 0,
    ctor:function(){
        this._super();
        this.init();
    },
    init: function () {

        g_sharedGameLayer = this;

        GC.CONTAINER.BOXES = [];
        GC.CONTAINER.BONUSES = [];
        GC.CONTAINER.EXPLOSIONS = [];

        GC.SCORE = 0;
        GC.CLICKS = GC.CLICKSDEFAULT;

        this._state = STATE_PLAYING;

        cc.spriteFrameCache.addSpriteFrames(res.explosion_plist);
        var explosionTexture = cc.textureCache.addImage(res.explosion_png);
        this._explosions = new cc.SpriteBatchNode(explosionTexture);
        this._explosions.setBlendFunc(cc.SRC_ALPHA, cc.ONE);
        this.addChild(this._explosions, 5000, GC.TAG.EXPLOSIONS);
        Explosion.sharedExplosion();
        Explosion.preSet();


        cc.spriteFrameCache.addSpriteFrames(res.assets_plist);
        var assets = cc.textureCache.addImage(res.assets_png);
        this._uiBatch = new cc.SpriteBatchNode(assets);
        this.addChild(this._uiBatch, 2000);

        winSize = cc.director.getWinSize();

        var topBackground = new cc.Sprite("#TOPprogress.PNG");
        topBackground.attr({
            anchorX: 0,
            anchorY: 1,
            x: 0,
            y: winSize.height,
            scale: winSize.width / topBackground.getContentSize().width
        });
        this._uiBatch.addChild(topBackground, 50, GC.TAG.TOPPROGRESS);

        this._progressBack = new ProgressBar(this,
            new cc.Sprite("#backLeftProgressBar.PNG"),
            new cc.Sprite("#backCenterProgressBar.PNG"),
            new cc.Sprite("#backRightProgressBar.PNG"),
            winSize.width / 3 - 2,
            winSize.height - 37,
            winSize.width / 3,
            0.21
        );

        this._uiBatch.addChild(this._progressBack._leftSprite, 51, GC.TAG.PROGRESSBARBACK);
        this._uiBatch.addChild(this._progressBack._centerSprite, 51, GC.TAG.PROGRESSBARBACK);
        this._uiBatch.addChild(this._progressBack._rightSprite, 51, GC.TAG.PROGRESSBARBACK);

        this._progressBar = new ProgressBar(this,
            new cc.Sprite("#LeftProgressBar.PNG"),
            new cc.Sprite("#CenterProgressBar.PNG"),
            new cc.Sprite("#RightProgressBar.PNG"),
            winSize.width / 3,
            winSize.height - 35,
            winSize.width / 3,
            0.2
        );
        this._uiBatch.addChild(this._progressBar._leftSprite, 52, GC.TAG.PROGRESSBAR)
        this._uiBatch.addChild(this._progressBar._centerSprite, 52, GC.TAG.PROGRESSBAR);
        this._uiBatch.addChild(this._progressBar._rightSprite, 52, GC.TAG.PROGRESSBAR);
                
        var padding = 30;
        var nBonuses = 3;
        var sizeBonuse = cc.spriteFrameCache.getSpriteFrame("BtnBox.PNG").getOriginalSize();
        var scaleBonuse = winSize.width / (sizeBonuse.width + padding * nBonuses)/4;
        for (var i = 1; i <= nBonuses; i++)
        Bonus.preSet(i * winSize.width / 4,
            sizeBonuse.height * scaleBonuse / 2 + padding,
            function () { GC.SCORE += 200; },
            scaleBonuse);

        var mainSprite = new cc.Sprite("#Main.PNG");
        var sizeMain = cc.spriteFrameCache.getSpriteFrame("Main.PNG").getOriginalSize();
        var maxHeightBonuses = (sizeBonuse.height * scaleBonuse + padding * 2);
        var maxHeightMain = (winSize.height - maxHeightBonuses - (topBackground.getContentSize().height * topBackground.scaleY + padding));
        var orientationMain = maxHeightMain < sizeMain.width;
        mainSprite.attr({
            anchorX: 0.5,
            anchorY: orientationMain ? 0 : 0.5,
            x: winSize.width / 2,
            y: orientationMain ? maxHeightBonuses : (maxHeightBonuses + maxHeightMain/2),
            scale: orientationMain ? (maxHeightMain / mainSprite.getContentSize().height) : (winSize.width / mainSprite.getContentSize().width)
        });

        this._uiBatch.addChild(mainSprite, 40, GC.TAG.MAIN);



        this._scoreSprite = new cc.Sprite("#InfoBlue.PNG");
        this._scoreSprite.attr({
            anchorX: 0,
            anchorY: 1,
            x: 5,
            y: winSize.height - 2,
            scale: (0.35 * topBackground.getContentSize().height * topBackground.scaleY) / this._scoreSprite.getContentSize().height
        });
        this._uiBatch.addChild(this._scoreSprite, 55, GC.TAG.SCORE);

        this._lbScore = new cc.LabelTTF("0", "Arial", 72);
        this._lbScore.setColor(cc.color(GC.FONTCOLOR));
        this._lbScore.textAlign = cc.TEXT_ALIGNMENT_CENTER;
        this._scoreSprite.addChild(this._lbScore, 2);
        this._lbScore.attr({
            anchorX: 0.5,
            anchorY: 0.6,
            x: this._lbScore.parent.getContentSize().width * 2 / 3,
            y: this._lbScore.parent.getContentSize().height / 2,
            scale: 1.5
        });

        this._clicksSprite = new cc.Sprite("#InfoRed.PNG");
        this._clicksSprite.attr({
            anchorX: 0,
            anchorY: 1,
            x: 5,
            y: winSize.height - 4 - this._scoreSprite.getContentSize().height * this._scoreSprite.scaleY,
            scale: (0.4 * topBackground.getContentSize().height * topBackground.scaleY) / this._clicksSprite.getContentSize().height
        });
        this._uiBatch.addChild(this._clicksSprite, 55, GC.TAG.CLICKS);

        this._lbClicks = new cc.LabelTTF("0", "Arial", 72);
        this._lbClicks.setColor(cc.color(GC.FONTCOLOR));
        this._lbClicks.textAlign = cc.TEXT_ALIGNMENT_CENTER;
        this._clicksSprite.addChild(this._lbClicks, 2);
        this._lbClicks.attr({
            anchorX: 0.5,
            anchorY: 0.7,
            x: this._lbClicks.parent.getContentSize().width * 2/ 3,
            y: this._lbClicks.parent.getContentSize().height / 2,
            scale: 1.5
        });


        this._pauseSprite = new cc.Sprite("#BtnPause.PNG");
        this._pauseSprite.attr({
            anchorX: 1,
            anchorY: 1,
            x: winSize.width - 15,
            y: winSize.height - 5,
            scale: (0.7 * topBackground.getContentSize().height * topBackground.scaleY) / this._pauseSprite.getContentSize().height
        });
        this._uiBatch.addChild(this._pauseSprite, 55, GC.TAG.PAUSE);

        this._gameoverSprite = new cc.Sprite("#Timer.PNG");
        this._gameoverSprite.attr({
            anchorX: 0.5,
            anchorY: 0.5,
            x: winSize.width/2,
            y: winSize.height/2,
            scale: (0.8 * winSize.width) / this._gameoverSprite.getContentSize().width
        });
        this._uiBatch.addChild(this._gameoverSprite, 8000, GC.TAG.GAMEOVER);

        this._lbGameover = new cc.LabelTTF("PAUSE", "Arial", 72);
        this._lbGameover.setColor(cc.color(GC.FONTCOLOR));
        this._lbGameover.textAlign = cc.TEXT_ALIGNMENT_CENTER;
        this._gameoverSprite.addChild(this._lbGameover, 2);
        this._lbGameover.attr({
            anchorX: 0.5,
            anchorY: 0.5,
            x: this._lbGameover.parent.getContentSize().width / 2,
            y: this._lbGameover.parent.getContentSize().height * 2 / 3,
            scale: 1.5
        });

        this._lbScoreGameover = new cc.LabelTTF("0", "Arial", 72);
        this._lbScoreGameover.setColor(cc.color(GC.FONTCOLOR));
        this._lbScoreGameover.textAlign = cc.TEXT_ALIGNMENT_CENTER;
        this._gameoverSprite.addChild(this._lbScoreGameover, 2);
        this._lbScoreGameover.attr({
            anchorX: 0.5,
            anchorY: 0.5,
            x: this._lbScoreGameover.parent.getContentSize().width / 2,
            y: this._lbScoreGameover.parent.getContentSize().height / 6,
            scale: 1.5
        });

        this._gameoverSprite.visible = false;

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        this._main = new GameManager(mainSprite, 7, 8, 3, 5, 5);
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        this._main.loadGame();

        if (GC.SOUND)
            cc.audioEngine.playMusic(cc.sys.os == cc.sys.OS_WP8 || cc.sys.os == cc.sys.OS_WINRT ? res.bgMusic_wav : res.bgMusic_mp3, true);

        this.scheduleUpdate();
        if (cc.sys.capabilities.hasOwnProperty('mouse')) {
            cc.eventManager.addListener({
                    event: cc.EventListener.MOUSE,
                    onMouseDown: function (event) {
                        if (event.getButton() == cc.EventMouse.BUTTON_LEFT) {
                            event.getCurrentTarget().checkIsCollide(event.getLocationX(), event.getLocationY());
                        }
                    }
            }, this);
        }

        return true;
    },
    checkIsCollide: function (x, y) {
        if (this._state == STATE_PLAYING) {
            var selChild = null;
            for (i = 0; i < GC.CONTAINER.BOXES.length; i++) {
                selChild = GC.CONTAINER.BOXES[i];
                if (!selChild.active)
                    continue;
                if (this.collide(selChild, cc.p(x, y))) {
                    this._main.clickOnBox(selChild);
                }
            }
            selChild = null;
            for (i = 0; i < GC.CONTAINER.BONUSES.length; i++) {
                selChild = GC.CONTAINER.BONUSES[i];
                if (!selChild.active)
                    continue;
                if (this.collide(selChild, cc.p(x, y))) {
                    selChild.doAction();
                }
            }
        }
        if (this._state == STATE_GAMEOVER) {
            cc.audioEngine.stopMusic();
            cc.audioEngine.stopAllEffects();
            var scene = new cc.Scene();
            scene.addChild(new Menu());
            cc.director.runScene(new cc.TransitionFade(1.2, scene));
        }
        if (this.collide(this._pauseSprite, cc.p(x, y))) {
            this._state = STATE_GAMEOVER;
        }
    },
    collide: function (a, b) {
        var ax = a.x, ay = a.y, bx = b.x, by = b.y;
        if (Math.abs(ax - bx) > MAX_CONTAINT_WIDTH || Math.abs(ay - by) > MAX_CONTAINT_HEIGHT)
            return false;

        var aRect = a.getBoundingBox();
        
        var bRect = cc.rect(bx, by, 1, 1);
        return cc.rectIntersectsRect(aRect, bRect);
    },
    updateUI: function () {
        if (this._tmpScore < GC.SCORE) {
            this._tmpScore += (GC.SCORE - this._tmpScore) * 0.1;
        }
        this._lbClicks.setString(GC.CLICKS + '');
        this._lbScore.setString('' + Math.floor(this._tmpScore));
        this._progressBar.setAmount(cc.pClamp(cc.p(this._tmpScore / GC.MAXPROGRESS, 0.0), cc.p(0.0, 0.0), cc.p(1.0, 0.0)).x);
    },
    update: function (dt) {
        if (this._state == STATE_GAMEOVER) {
            this._lbScoreGameover.setString(GC.SCORE);
            this._gameoverSprite.visible = true;
        }
        if (this._state == STATE_PLAYING) {
            if (this.checkGameover()) {
                this._state = STATE_GAMEOVER;
            }
        }
        this.updateUI();
    },
    checkGameover: function () {
        if (this._tmpScore >= GC.MAXPROGRESS) {
            this._lbGameover.setString('WIN');
            this.game
            return true;
        }
        if (GC.CLICKS <= 0 || this._main._gameoverFlag == true) {
            this._lbGameover.setString('LOSE');
            return true;
        }
        return false;
    }
});

GameLayer.scene = function () {
    var scene = new cc.Scene();
    var layer = new GameLayer();
    scene.addChild(layer, 1);
    return scene;
};

GameLayer.prototype.addBonus = function (bonus, z, tag) {
    this._uiBatch.addChild(bonus, z, tag);
};
GameLayer.prototype.addBox = function (box, z, tag) {
    this._uiBatch.addChild(box, z, tag);
};
GameLayer.prototype.addExplosions = function (explosion) {
    this._explosions.addChild(explosion);
};