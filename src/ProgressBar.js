/**
 * ����� ProgressBar ������ ��� ������� ������ ProgressBar'�
 * � �������� ������ ������������ � ����������� �� �������� ���������.
 * @param {object} _gameLayer ������ �� ������� ����.
 * @param {number} _amount �������� ������������� �� 0.0 �� 1.0.
 * @param {number} _x ���������� X ��� ProgressBar.
 * @param {number} _y ���������� Y ��� ProgressBar.
 * @param {number} _anchor �����, ���������������� ��������� � ���������� ������� ProgressBar.
 * @param {number} _maxSize ������ ProgressBar'�
 * @param {number} _scale ������� ������������ ������������� �������.
 * @param {object} _leftSprite ����� ����������� - �� ���������������.
 * @param {object} _centerSprite ����������� ����������� - ���������������.
 * @param {object} _rightSprite ������ ����������� - �� ���������������.
 */


var ProgressBar = cc.Class.extend({
    _gameLayer: null,
    _amount: null,
    _x: null,
    _y: null,
    _anchor: null,
    _maxSize: null,
    _scale: null,
    _leftSprite: null,
    _centerSprite: null,
    _rightSprite: null,

    ctor: function (gameLayer, l, c, r, x = 0, y = 0, maxSize = 100, scale = 1, anchor = GC.PROGRESSBAR.BOTTOM_leftSpriteEFT, amount = 1.0) {
        if (!gameLayer) {
            throw "oops... gameLayer is not here!";
        }
        this._gameLayer = gameLayer;
        this._amount = amount;
        this._anchor = anchor;
        this._maxSize = maxSize;
        this._x = x;
        this._y = y;
        this._scale = scale;
        this._leftSprite = l;
        this._centerSprite = c;
        this._rightSprite = r;
        this.updateSprites();
    },
    setAmount: function (amount) {
        this._amount = amount;
        this.updateSprites();
    },
    updateSprites: function () {
        if (this._anchor == GC.PROGRESSBAR.BOTTOM_leftSpriteEFT) {
            this._leftSprite.attr({
                anchorX: 0,
                anchorY: 0,
                x: this._x,
                y: this._y,
                scale: this._scale,
            });
            var leftSize = this._leftSprite.getContentSize().width * this._leftSprite.scaleX;
            this._centerSprite.attr({
                anchorX: 0,
                anchorY: 0,
                x: this._x + leftSize,
                y: this._y,
                scaleY: this._scale,
                scaleX: this._amount * this._maxSize / this._centerSprite.getContentSize().width
            });
            var centerSize = this._centerSprite.getContentSize().width * this._centerSprite.scaleX;
            this._rightSprite.attr({
                anchorX: 0,
                anchorY: 0,
                x: this._x + leftSize + centerSize,
                y: this._y,
                scale: this._scale,
            });
        }
    },
});