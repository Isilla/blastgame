var BoxType = [
    {
        type: 0,
        textureName: "BoxBlue.PNG",
        scoreValue: 100
    },
    {
        type: 1,
        textureName: "BoxGreen.PNG",
        scoreValue: 222
    },
    {
        type: 2,
        textureName: "BoxPurple.PNG",
        scoreValue: 444
    },
    {
        type: 3,
        textureName: "BoxRed.PNG",
        scoreValue: 300
    },
    {
        type: 4,
        textureName: "BoxYellow.PNG",
        scoreValue: 200
    },
];
