var GC = GC || {};

//����
GC.SCORE = 0;

//���������� ���������� ������
GC.CLICKS = 10;
GC.CLICKSDEFAULT = 50;

//����������� ���������� ����� ��� ��������
GC.MAXPROGRESS = 50000;

//���. / ����. ����
GC.SOUND = false;

//����������
GC.CONTAINER = {
    BOXES: [],
    BONUSES: [],
    EXPLOSIONS:[],
    SPARKS:[],
};

//�������
GC.PROGRESSBAR = {
    BARS:[],
    CENTER: 0,
    BOTTOM_LEFT: 1,
    LEFT: 2,
    CENTER: 3,
    RIGHT: 4,
};

//����
GC.TAG = {
    BOX: 1,
    MAIN: 2,
    PROGRESSBAR: 3,
    PROGRESSBARBACK: 4,
    TOPPROGRESS: 5,
    CLICKS: 6,
    SCORE: 7,
    PAUSE: 8,
    GAMEOVER:9,
    BONUSE:10,
};

//���������� �������� �������
GC.ACTIVE_BOXES = 0;

//������ / ������ ����
GC.WIDTH = 480;
GC.HEIGHT = 720;

//���� �������
GC.FONTCOLOR = "#fffff1";
