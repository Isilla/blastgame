
var res = {
    assets_plist: 'res/a.plist',
    assets_png: 'res/a.png',
    bgMusic_mp3: 'res/Music/bgMusic.mp3',
    bgMusic_ogg: 'res/Music/bgMusic.ogg',
    bgMusic_wav: 'res/Music/bgMusic.wav',
    buttonEffet_mp3: 'res/Music/buttonEffet.mp3',
    buttonEffet_ogg: 'res/Music/buttonEffet.ogg',
    buttonEffet_wav: 'res/Music/buttonEffet.wav',
    explodeEffect_mp3: 'res/Music/explodeEffect.mp3',
    explodeEffect_ogg: 'res/Music/explodeEffect.ogg',
    explodeEffect_wav: 'res/Music/explodeEffect.wav',
    fireEffect_mp3: 'res/Music/fireEffect.mp3', 
    fireEffect_ogg: 'res/Music/fireEffect.ogg', 
    fireEffect_wav: 'res/Music/fireEffect.wav', 
    mainMainMusic_mp3: 'res/Music/mainMainMusic.mp3',
    mainMainMusic_ogg: 'res/Music/mainMainMusic.ogg',
    mainMainMusic_wav: 'res/Music/mainMainMusic.wav',
    shipDestroyEffect_mp3: 'res/Music/shipDestroyEffect.mp3',
    shipDestroyEffect_ogg: 'res/Music/shipDestroyEffect.ogg',
    shipDestroyEffect_wav: 'res/Music/shipDestroyEffect.wav',
    arial_14_fnt: 'res/arial-14.fnt',
    arial_14_png: 'res/arial-14.png',
    explosion_plist: 'res/explosion.plist',
    explosion_png: 'res/explosion.png'
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
