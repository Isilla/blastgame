var Box = cc.Sprite.extend({
    _gridX: null,
    _gridY: null,
    active: true,
    zOrder: 1000,
    action: null,
    boxType: 0,
    scoreValue: 0,
    ctor: function (arg, x, y) {
        this._super("#" + arg.textureName);
        this.boxType = arg.type;
        this.scoreValue = arg.scoreValue;
        this.scale = 0.2;
        this._gridX = x;
        this._gridY = y;
    },
    collideRect: function (x, y) {
        var w = this.width, h = this.height;
        return cc.rect(x - w / 2, y - h / 2, w, h);
    },
    destroy: function (isScore = true) {
        if (isScore)
            GC.SCORE += this.scoreValue;

        var explosion = Explosion.getOrCreateExplosion();
        explosion.x = this.x;
        explosion.y = this.y;
        if (GC.SOUND) {
            cc.audioEngine.playEffect(cc.sys.os == cc.sys.OS_WP8 || cc.sys.os == cc.sys.OS_WINRT ? res.explodeEffect_wav : res.explodeEffect_mp3);
        }
        this.visible = false;
        this.active = false;
        this.stopAllActions();
        
        GC.ACTIVE_BOXES--;
    },

});

Box.create = function (arg, x = 0, y = 0) {
    var box = new Box(arg, x, y);
    g_sharedGameLayer.addBox(box, box.zOrder, GC.TAG.BOX);
    GC.CONTAINER.BOXES.push(box);
    return box;
};

Box.getOrCreateBox = function (arg, x, y) {
    var selChild = null;
    for (var j = 0; j < GC.CONTAINER.BOXES.length; j++) {
        selChild = GC.CONTAINER.BOXES[j];

        if (selChild.active == false && selChild.boxType == arg.type) {
            selChild.active = true;
            selChild.visible = true;
            selChild._gridX = x;
            selChild._gridY = y;
            GC.ACTIVE_BOXES++;
            return selChild;
        }
    }
    selChild = Box.create(arg, x, y);
    GC.ACTIVE_BOXES++;
    return selChild;
};

Box.getBoxByXY = function (x, y) {
    var selChild = null;
    for (var j = 0; j < GC.CONTAINER.BOXES.length; j++) {
        selChild = GC.CONTAINER.BOXES[j];
        if (selChild.active == true && selChild._gridX == x && selChild._gridY == y) {
            return selChild;
        }
    }
    return null;
};