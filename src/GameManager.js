/**
 * ����� GameManager ������ � ��������� ������� ����.
 * 
 * TODO: �������������� ����� amountPoint ��� ��� ��������� ������ �������
 * TODO: �������������� ����� destroyPoints ��� ��� ��������� ������ �������
 * 
 * @param {object} _gameLayer ������ �� ������� ����.
 * @param {array|object} _map ��������� ������ ���������� ���� �������, >0 - ������� �������, ==0 - ������ �����, <0 - ������ �������.
 * @param {boolean} _gameoverFlag ���� ���������� �� �������� ��� ������������� �������� ������ ��������� �� _s ���������.
 * @param {number} _n ������ ����� �����.
 * @param {number} _m ������ ����� �����.
 * @param {number} _k ����������� ���������� ������ ��� ��������.
 * @param {number} _c ���������� ������.
 * @param {number} _s ���������� ��������� ��� ��������� ��� ����������� �����.
 * @param {function} amountPoint ������������ ���������� ������� � ���������� ����� ��������� �������.
 * @param {function} loadGame �������� ���� �������� ����� ������ ���������.
 */
var GameManager = cc.Class.extend({
    _gameLayer: null,
    _map: null,
    _gameoverFlag: false,
    _n: 15,
    _m: 10,
    _k: 3,
    _c: 5,
    _s: 5,
    ctor: function (gameLayer, n=8, m=7, k=3, c=5, s=5) {
        if (!gameLayer) {
            throw "oops... gameLayer is not here!";
        }
        this._gameLayer = gameLayer;
        this._n = n;
        this._m = m;
        this._k = k;
        this._c = c;
        this._s = s;

        this._map = Array.from(Array(this._n), () => new Array(this._m));
        this.generateMap();
    },
    generateMap: function () {
        for (var i = 0; i < this._n; i++) {
            for (var j = 0; j < this._m; j++) {
                this._map[i][j] = Math.floor(Math.random() * this._c) + 1;
            }
        }
        var sum = 1;
        if (this.isDead())
            sum += this.generateMap();
        if (sum >= this._s) {
            this._gameoverFlag = true;
        }
        return sum;
    },
    isDead: function () {
        var mineMap = Array.from(Array(this._n), () => new Array(this._m));
        for (var i = 0; i < this._n; i++) {
            for (var j = 0; j < this._m; j++) {
                mineMap[i][j] = 0;
            }
        }
        for (var i = 0; i < this._n; i++) {
            for (var j = 0; j < this._m; j++) {
                if (this.amountPoint(mineMap.slice(), j, i, this._map[i][j]) >= this._k)
                    return false;
            }
        }
        return true;
    },
    amountPoint: function (mineMap, x, y, type) {
        if (mineMap[y][x] == 1 || this._map[y][x] != type || this._map[y][x] <= 0)
            return 0;
        mineMap[y][x] = 1;
        var sum = 1;
        if (this.isInBound(x + 1, y))
            sum += this.amountPoint(mineMap, x + 1, y, type);
        if (this.isInBound(x - 1, y))
            sum += this.amountPoint(mineMap, x - 1, y, type);
        if (this.isInBound(x, y + 1))
            sum += this.amountPoint(mineMap, x, y + 1, type);
        if (this.isInBound(x, y - 1))
            sum += this.amountPoint(mineMap, x, y - 1, type);
        return sum;
    },
    isInBound: function (x, y) {
        if (x >= 0 && x < this._m && y >= 0 && y < this._n) {
            return true;
        }
        return false;
    },
    loadGame: function () {
        for (var i = 0; i < this._n; i++) {
            for (var j = 0; j < this._m; j++) {
                if (this._map[i][j] <= 0)
                    continue;
                var addBox = Box.getOrCreateBox(BoxType[(this._map[i][j] - 1) % Math.min(this._c, BoxType.length)], j, i);
                var padding = 30;
                var scalX = (this._gameLayer.getContentSize().width * this._gameLayer.scaleX - padding) / this._m / addBox.getContentSize().width;
                var scalY = (this._gameLayer.getContentSize().height * this._gameLayer.scaleY - padding) / this._n / addBox.getContentSize().height;
                addBox.attr({
                    x: j * addBox.getContentSize().width * scalX + addBox.getContentSize().width * scalX / 2 + this._gameLayer.x - (this._gameLayer.getContentSize().width  * this._gameLayer.scaleX - padding) / 2,
                    y: padding/2 - i * addBox.getContentSize().height * scalY - addBox.getContentSize().height * scalY / 2 + this._gameLayer.y + (this._gameLayer.getContentSize().height  * this._gameLayer.scaleY - padding) * (1 - this._gameLayer.anchorY),
                    scaleX: scalX,
                    scaleY: scalY
                });
                
            }
        }
    },

    clickOnBox: function (selChild) {
        var x = selChild._gridX;
        var y = selChild._gridY;
        var mineMap = Array.from(Array(this._n), () => new Array(this._m));

        for (var i = 0; i < this._n; i++) {
            for (var j = 0; j < this._m; j++) {
                mineMap[i][j] = 0;
            }
        }

        var amount = this.amountPoint(mineMap, x, y, this._map[y][x]);
        for (var i = 0; i < this._n; i++) {
            for (var j = 0; j < this._m; j++) {
                mineMap[i][j] = 0;
            }
        }
        if (amount >= this._k) {
            GC.CLICKS--;
            this.destroyPoints(mineMap, x, y, this._map[y][x]);
            this.moveAfterDestroy();
        }
    },
    destroyPoints: function (mineMap, x, y, type) {
        if (mineMap[y][x] == 1 || (this._map[y][x] != type)) {
            return;
        }

        mineMap[y][x] = 1;
        this._map[y][x] = 0;
        var delBox = Box.getBoxByXY(x, y);
        delBox.destroy();

        if (this.isInBound(x + 1, y))
            this.destroyPoints(mineMap, x + 1, y, type);
        if (this.isInBound(x - 1, y))
            this.destroyPoints(mineMap, x - 1, y, type);
        if (this.isInBound(x, y + 1))
            this.destroyPoints(mineMap, x, y + 1, type);
        if (this.isInBound(x, y - 1))
            this.destroyPoints(mineMap, x, y - 1, type);
        return;
        
    },
    moveAfterDestroy: function () {
        for (var j = 0; j < this._m; j++) {
            var ii = this._n - 1;
            for (var i = this._n-1; i >=0; i--) {
                if (this._map[i][j] == 0) {
                    continue;
                }
                if (ii != i) {
                    this._map[ii][j] = this._map[i][j];
                    this._map[i][j] = 0;
                    var selChild = Box.getBoxByXY(j, i);
                    selChild._gridY = ii;
                }
                ii--;
            }
        }
        this.updateMap();
        this.fillEmpty();
        if (this.isDead()) {
            this.destroyAll();
            this.generateMap();
            this.loadGame();
        }
    },
    fillEmpty: function () {
        for (var i = 0; i < this._n; i++) {
            for (var j = 0; j < this._m; j++) {
                if (this._map[i][j] <= 0) {
                    this._map[i][j] = Math.floor(Math.random() * this._c) + 1;
                    var addBox = Box.getOrCreateBox(BoxType[(this._map[i][j] - 1) % Math.min(this._c, BoxType.length)], j, i);
                    if (addBox == null)
                        continue;
                    var padding = 30;
                    var scalX = (this._gameLayer.getContentSize().width * this._gameLayer.scaleX - padding) / this._m / addBox.getContentSize().width;
                    var scalY = (this._gameLayer.getContentSize().height * this._gameLayer.scaleY - padding) / this._n / addBox.getContentSize().height;
                    addBox.attr({
                        scaleX: scalX,
                        scaleY: scalY,
                        x: j * addBox.getContentSize().width * scalX + addBox.getContentSize().width * scalX / 2 + this._gameLayer.x - (this._gameLayer.getContentSize().width * this._gameLayer.scaleX - padding) / 2,
                        y: padding / 2 - (-2) * addBox.getContentSize().height * scalY - addBox.getContentSize().height * scalY / 2 + this._gameLayer.y + (this._gameLayer.getContentSize().height * this._gameLayer.scaleY - padding) * (1 - this._gameLayer.anchorY)
                    });
                    var x = j * addBox.getContentSize().width * scalX + addBox.getContentSize().width * scalX / 2 + this._gameLayer.x - (this._gameLayer.getContentSize().width * this._gameLayer.scaleX - padding) / 2;
                    var y = padding / 2 - i * addBox.getContentSize().height * scalY - addBox.getContentSize().height * scalY / 2 + this._gameLayer.y + (this._gameLayer.getContentSize().height * this._gameLayer.scaleY - padding) * (1 - this._gameLayer.anchorY);
                    var action = cc.moveTo(0.2, cc.p(x, y));
                    addBox.runAction(action);
                }
            }
        }
    },
    destroyAll: function () {
        for (var i = 0; i < this._n; i++) {
            for (var j = 0; j < this._m; j++) {
                var addBox = Box.getBoxByXY(j, i);
                if (addBox == null)
                    continue;
                addBox.destroy(false);
            }
        }
    },
    updateMap: function () {
        for (var i = 0; i < this._n; i++) {
            for (var j = 0; j < this._m; j++) {
                var addBox = Box.getBoxByXY(j, i);
                if (addBox == null)
                    continue;
                addBox.stopAllActions();
                var padding = 30;
                var scalX = (this._gameLayer.getContentSize().width * this._gameLayer.scaleX - padding) / this._m / addBox.getContentSize().width;
                var scalY = (this._gameLayer.getContentSize().height * this._gameLayer.scaleY - padding) / this._n / addBox.getContentSize().height;
                addBox.attr({
                    scaleX: scalX,
                    scaleY: scalY
                });
                var x = j * addBox.getContentSize().width * scalX + addBox.getContentSize().width * scalX / 2 + this._gameLayer.x - (this._gameLayer.getContentSize().width * this._gameLayer.scaleX - padding) / 2;
                var y = padding / 2 - i * addBox.getContentSize().height * scalY - addBox.getContentSize().height * scalY / 2 + this._gameLayer.y + (this._gameLayer.getContentSize().height * this._gameLayer.scaleY - padding) * (1 - this._gameLayer.anchorY);
                var action = cc.moveTo(0.2, cc.p(x, y));
                addBox.runAction(action);
            }
        }
    }
});