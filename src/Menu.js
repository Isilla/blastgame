/**
 * ���� Menu ��� ������� ���� ���������� ������� �� ������� �����
 * � ���������� �������� ��������.
 */

var Menu = cc.Layer.extend({

    ctor: function () {
        this._super();
        this.init();
    },
    init: function () {
        cc.spriteFrameCache.addSpriteFrames(res.assets_plist);
        winSize = cc.director.getWinSize();

        var assets = cc.textureCache.addImage(res.assets_png);
        this._assetsBatch = new cc.SpriteBatchNode(assets);
        this.addChild(this._assetsBatch);

        var lbTitle = new cc.LabelTTF("Blast", "Arial", 72);
        lbTitle.setColor(cc.color(GC.FONTCOLOR));
        lbTitle.textAlign = cc.TEXT_ALIGNMENT_CENTER;
        this.addChild(lbTitle);
        lbTitle.attr({
            anchorX: 0.5,
            anchorY: 1,
            x: winSize.width / 2,
            y: winSize.height-100,
            scale: 2
        });
        
        var newGameNormal1 = new cc.Sprite("#BtnBlue.PNG");
        newGameNormal1.attr({
            anchorX: 0,
            anchorY: 0,
            x: 0,
            y: 0
        });

        var newGame1 = new cc.MenuItemSprite(newGameNormal1, newGameNormal1.clone, newGameNormal1.clone, function () {
            this.onButtonEffect();
            var onComplete = cc.callFunc(this.onNewGame, this);
            this.runAction(cc.sequence(GC.SOUND ? cc.delayTime(2) : cc.delayTime(0), onComplete));
        }.bind(this), this);
        newGame1.scale = 0.5;

        var label = new cc.LabelTTF("Play", "Arial", 72);
        label.setColor(cc.color(GC.FONTCOLOR));
        label.textAlign = cc.TEXT_ALIGNMENT_CENTER;
        newGame1.addChild(label, 2);
        label.attr({
            anchorX: 0.5,
            anchorY: 0.5,
            x: label.parent.getContentSize().width/2,
            y: label.parent.getContentSize().height / 2,
            scale: 1.5
        });

        cc.MenuItemFont.setFontName("Arial");
        cc.MenuItemFont.setFontSize(18);
        var title1 = new cc.MenuItemFont("Sound: ");
        title1.setEnabled(false);
        title1.setColor(cc.color(GC.FONTCOLOR));

        cc.MenuItemFont.setFontName("Arial");
        cc.MenuItemFont.setFontSize(26);
        var item1 = new cc.MenuItemToggle(
            new cc.MenuItemFont("On"), new cc.MenuItemFont("Off"));
        item1.setCallback(this.onSoundControl);
        item1.setColor(cc.color(GC.FONTCOLOR));
        var state = GC.SOUND ? 0 : 1;
        item1.setSelectedIndex(state);

        var menu = new cc.Menu(newGame1, title1, item1);
        menu.alignItemsVerticallyWithPadding(15);
        this.addChild(menu,1,2);
        menu.x = winSize.width / 2;
        menu.y = winSize.height / 2;
        menu.alignItemsInColumns(1, 2);

        if (GC.SOUND) {
            cc.audioEngine.setMusicVolume(0.7);
            cc.audioEngine.playMusic(cc.sys.os == cc.sys.OS_WP8 || cc.sys.os == cc.sys.OS_WINRT ? res.mainMainMusic_wav : res.mainMainMusic_mp3, true);
        }

        return true;
    },
    onNewGame: function (pSender) {
        cc.LoaderScene.preload(g_resources, function () {
            cc.audioEngine.stopMusic();
            cc.audioEngine.stopAllEffects();
            var scene = new cc.Scene();
            scene.addChild(new GameLayer());
            cc.director.runScene(new cc.TransitionFade(1.2, scene));
        }, this);
    },
    onButtonEffect: function () {
        if (GC.SOUND) {
            var s = cc.audioEngine.playEffect(cc.sys.os == cc.sys.OS_WP8 || cc.sys.os == cc.sys.OS_WINRT ? res.buttonEffet_wav : res.buttonEffet_mp3);
        }
    },
    onSoundControl: function () {
        GC.SOUND = !GC.SOUND;
        var audioEngine = cc.audioEngine;
        if (GC.SOUND) {
            audioEngine.playMusic(cc.sys.os == cc.sys.OS_WP8 || cc.sys.os == cc.sys.OS_WINRT ? res.mainMainMusic_wav : res.mainMainMusic_mp3);
        }
        else {
            audioEngine.stopMusic();
            audioEngine.stopAllEffects();
        }
    },
});

Menu.scene = function () {
    var scene = new cc.Scene();
    var layer = new Menu();
    scene.addChild(layer);
    return scene;
};
