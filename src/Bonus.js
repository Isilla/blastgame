var Bonus = cc.Sprite.extend({
    active: true,
    zOrder: 1000,
    action: null,
    ctor: function (x, y, fun = function () { }, scale = 1) {
        this._super("#BtnBox.PNG");
        
        this.action = fun;
        this.x = x;
        this.y = y;
        this.scale = scale;
    },
    collideRect: function (x, y) {
        var w = this.width, h = this.height;
        return cc.rect(x - w / 2, y - h / 2, w, h);
    },
    setAction: function (f) {
        this.action = f;
    },
    doAction() {
        this.action();
    }
});
Bonus.create = function (x, y, fun = function () { }, scale = 1) {
    var bonus = new Bonus(x, y, fun, scale);
    g_sharedGameLayer.addBonus(bonus, bonus.zOrder, GC.TAG.BONUSE);
    GC.CONTAINER.BONUSES.push(bonus);
    return bonus;
};
Bonus.preSet = function (x, y, fun = function () { }, scale = 1) {
    bonus = Bonus.create(x, y, fun, scale);
};